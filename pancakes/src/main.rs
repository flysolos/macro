use hello_macro::HelloMacro;
use hello_macro_derive::HelloMacro;
// struct Pancakes;

#[derive(HelloMacro)]
struct killer;

fn main() {
    killer::hello_macro();
}
