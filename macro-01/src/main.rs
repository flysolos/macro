macro_rules! yo {
    ($name:expr) => {
        println!("hello {} ",$name);
    };
}
fn main() {
    println!("Hello, world!");
    yo!("郭清华");
}
