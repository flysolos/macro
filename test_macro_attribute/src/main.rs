#[macro_use]
extern crate my_macro;

#[log_entry_and_exit(hello, "world")]
fn this_will_be_destroyed() -> i32 {
    42
}
my_macro::hw!();//新增一行调用代码，注意，一定要放在这里才不报错
fn main() {
    dummy()
}
