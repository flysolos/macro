extern crate proc_macro;

use crate::proc_macro::TokenStream;
use quote::quote;
use syn;


pub fn hello_macro_derive(input: TokenStream) -> TokenStream {
    // Construct a representation of Rust code as a syntax tree
    // that we can manipulate
    //从Rust代码构造出我们可以操作的语法树ast
    //文档：https://docs.rs/syn/0.14.4/syn/struct.DeriveInput.html
    let ast: syn::DeriveInput = syn::parse(input).unwrap();

    // 实现特征方法 Build the trait implementation
    impl_hello_macro(&ast)
}

// 实现特征方法 
fn impl_hello_macro(ast: &syn::DeriveInput) -> TokenStream {
    let name = &ast.ident;
    let gen = quote! {//主要用来替换相关字符串，生成特定代码,
        //文档：https://docs.rs/quote/1.0.2/quote/
        impl HelloMacro for #name {
            fn hello_macro() {
                println!("你好, 宏编程! 实体类名称为 {}", stringify!(#name));
            }
        }
    };
    gen.into()
}