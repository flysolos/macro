extern crate custom_derive;
use custom_derive::log_attr;
use custom_derive::make_hello;
use custom_derive::Hello;

make_hello!(world);
make_hello!(张三);
make_hello!(李四);

make_hello!(历史);

#[log_attr(struct, "world")]
struct Hello{
    pub name: String,
}

#[log_attr(func, "test")]
fn invoked(){}


#[derive(Hello)]
struct World;

fn main() {
    // 使用make_hello生成
    hello_world();
    hello_张三();
    hello_李四();
    hello_历史();
    hello_历史();
}