use trace_var::trace_var;

fn main() {
    println!("is tokens {}", factorial(18));
}

#[trace_var(p, n)]
fn factorial(mut n: u64) -> u64 {
    let mut p = 1;
    while n > 1 {
        p *= n;
        n -= 1;
    }
    p
}